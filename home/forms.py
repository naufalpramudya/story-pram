from django import forms
from .models import MataKuliah
#from .models import MataKuliah

class FormBiasa(forms.Form):
    nama_matkul = forms.CharField()
    dosen_pengajar = forms.CharField()
    sks = forms.CharField()
    semester = forms.CharField()
    ruangan = forms.CharField()
    deskripsi = forms.CharField(widget=forms.Textarea)

class FormMatkul(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = (
            'nama', 'dosen', 'sks', 'semester',
            'ruangan', 'deskripsi'
        )

class FormHapus(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = ('nama',)