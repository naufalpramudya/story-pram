from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('story1', views.story1, name='story1'),
    path('mail', views.mail, name='mail'),
    path('tambah_matkul', views.tambah_matkul, name='tambah_matkul'),
    path('list_matkul', views.list_matkul, name='list_matkul'),
    path('hapus_matkul', views.hapus_matkul, name='hapus_matkul')
]