from django.db import models

# Create your models here.

class MataKuliah(models.Model):
    nama = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=30)
    semester = models.CharField(max_length=30)
    ruangan = models.CharField(max_length=30)
    deskripsi = models.TextField()

    def __str__(self):
        return self.nama

