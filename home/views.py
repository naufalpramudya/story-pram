from django.shortcuts import render
from django.http import HttpResponse
from .forms import FormBiasa, FormMatkul, FormHapus
from .models import MataKuliah
# Create your views here.

response = {}

def index(request):
    return render(request, 'home/index.html')

def story1(request):
    return render(request, 'home/story1.html')

def mail(request):
    return render(request, 'home/mail.html')

def tambah_matkul(request):
    if request.method == 'POST':
        form = FormMatkul(request.POST)
        if form.is_valid():
            form.save()
            print(form)
            
    form = FormMatkul()
    return render(request, 'home/matkul.html', {'form' : form})

def list_matkul(request):
    list_matkul = MataKuliah.objects.all()
    response['list_matkul'] = list_matkul
    return render(request, 'home/list_matkul.html', response)

def hapus_matkul(request):
    
    if request.method == 'POST':
        form = FormHapus(request.POST)
        if form.is_valid():
            nama_del = request.POST['nama']
            for matkul in MataKuliah.objects.all():
                if matkul.nama == nama_del:
                    item = MataKuliah.objects.get(nama = nama_del)
                    item.delete()
                    break
                
    form = FormHapus()
    return render(request, 'home/hapus_matkul.html', {'form':form})

