from django.test import TestCase, Client
from django.urls import resolve
from .views import books


# Create your tests here.
class UnitTest(TestCase):
    def test_url_books(self):
        response = Client().get('/books/')
        self.assertEquals(response.status_code, 200)

    def test_function_books(self):
        view = resolve('/books/')
        self.assertEquals(view.func, books)

    def test_json_url(self):
        response = Client().get('/books/fetch/')
        self.assertEqual(response.status_code, 200)

    def test_template_books(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books/books.html')
