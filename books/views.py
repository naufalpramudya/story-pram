from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import render
import json
import requests

# Create your views here.


def books(request):
    return render(request, 'books/books.html')


def fetch(request):
    try:
        q = request.GET['q']
    except:
        q = "quilting"

    json_read = requests.get(
        'https://www.googleapis.com/books/v1/volumes?q=' + q).json()

    return JsonResponse(json_read)
