from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404
from .models import Aktivitas, Peserta
from .forms import FormAktivitas, FormPeserta
# Create your views here.

def list_aktivitas(request):
    list_aktivitas = Aktivitas.objects.all()
    list_peserta = Peserta.objects.all()
    form = FormAktivitas()
    form_peserta = FormPeserta()
    if request.method== "POST":
        form = FormAktivitas(request.POST)
        if form.is_valid():
            form.save()
            form = FormAktivitas()
    else:
        FormAktivitas()
    
    pass_arg =  {
        'form':form,
        'list_ak':list_aktivitas,
        'list_pes':list_peserta,
        'Ada':True
    }
    return render(request, 'kegiatan/list_aktivitas.html',context=pass_arg)

def tambah_peserta(request, id):
    list_peserta={}
    kegiatan = get_object_or_404(Aktivitas, pk=id)
    if request.method == 'POST':
       form_peserta = FormPeserta(request.POST)
       if form_peserta.is_valid():
            list_peserta = form_peserta.save(commit=False)
            list_peserta.kegiatan = kegiatan
            list_peserta.save()
            return redirect('/aktivitas/')
    else:
        form_peserta = FormPeserta()
        
    pass_args = {
        'peserta':list_peserta,
        'form':form_peserta
    }
    return render(request, 'kegiatan/tambah_peserta.html', context=pass_args)
