from django import forms
from .models import Aktivitas, Peserta


class FormAktivitas(forms.ModelForm):
    aktivitas = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto'
        }, 
    ))
    class Meta:
        model = Aktivitas
        fields = ('aktivitas',)


class FormPeserta(forms.ModelForm):
    peserta = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={
            'class' : 'form-control mx-auto'
        }, 
    ))
    class Meta:
        model = Peserta
        fields = ('peserta',)
    

