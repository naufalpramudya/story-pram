from django.urls import path

from . import views

app_name = 'kegiatan'

urlpatterns = [
    path('', views.list_aktivitas, name='list_aktivitas'),
    path('tambah_peserta/<int:id>', views.tambah_peserta, name='tambah_peserta'),
]