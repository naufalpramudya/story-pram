from django.db import models

# Create your models here.

class Aktivitas(models.Model):
    aktivitas = models.CharField(max_length=30)

class Peserta(models.Model):
    peserta = models.CharField(max_length=30)
    kegiatan = models.ForeignKey(Aktivitas, on_delete=models.CASCADE)