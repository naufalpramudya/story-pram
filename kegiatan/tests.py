from django.test import TestCase, Client
from django.urls import resolve
from .models import Aktivitas, Peserta
from .forms import FormAktivitas, FormPeserta
from .views import list_aktivitas
# Create your tests here.

class TestAktivitas(TestCase):
    
    #Setup
    def setUp(self):
        self.client = Client()
        self.aktivitas = Aktivitas.objects.create(aktivitas = "nonton")
        self.peserta = Peserta.objects.create(peserta = "pram", kegiatan=self.aktivitas)
    
    #Test Urls
    def test_url_list_aktivitas_ada(self):
        response = self.client.get('/aktivitas', {}, True)
        self.assertEqual(response.status_code, 200)
    

    #Test Templates
    def test_list_aktivitas_using_template(self):
        response = self.client.get('/aktivitas/')
        self.assertTemplateUsed(response, 'kegiatan/list_aktivitas.html')
    
    #Test Views
    def test_list_aktivitas_using_correct_views(self):
        view = resolve('/aktivitas/')
        self.assertEqual(view.func, list_aktivitas)
    
    def test_halaman_aktivitas_post_success_dan_berhasil_render_result(self):
        response_post = self.client.post(
            '/aktivitas/', {
                'aktivitas': 'Salto'
            }
        )
        self.assertEqual(response_post.status_code, 200)

    def test_tambah_peserta_post_not_exist_and_render_the_result(self):
        response_post = self.client.post(
            '/event/nambah/skuy', {
                'nama': 'ujuy'
            }
        )
        self.assertEqual(response_post.status_code, 404)
    
    def test_add_luffy_404_render_result(self):
        response_post = self.client.post(
            '/event/nambah/ayo', {
                'nama': 'luffy'
            }
        )
        self.assertEqual(response_post.status_code, 404)

    def test_add_naruto_404_render_result(self):
        response_post = self.client.post(
            '/event/nambah/konoha', {
                'nama': 'naruto'
            }
        )
        self.assertEqual(response_post.status_code, 404)
    
    def test_add_sowon_404_render_result(self):
        response_post = self.client.post(
            '/event/nambah/gap', {
                'nama': 'sowon'
            }
        )
        self.assertEqual(response_post.status_code, 404)

    def test_add_jonfer_404_render_result(self):
        response_post = self.client.post(
            '/event/nambah/yutub', {
                'nama': 'jonfer'
            }
        )
        self.assertEqual(response_post.status_code, 404)

    def test_add_here_a_404_render_result(self):
        response_post = self.client.post(
            '/event/nambah/adkez', {
                'nama': 'hira'
            }
        )
        self.assertEqual(response_post.status_code, 404)


    #Test Models
    def test_halaman_pakai_index_template_setelah_post(self):
        akt = Aktivitas.objects.create(aktivitas="fanboying",id=5)
        akt.save()
        response = self.client.post('/aktivitas/',{'id':5,'anggota':'fachri'})
        self.assertEqual(response.status_code, 200)

    def test_halaman_tambah_peserta(self):
        akt = Aktivitas.objects.create(aktivitas="konser",id=4)
        akt.save()
        response = self.client.post('/aktivitas/tambah_peserta/4',{'id':4,'anggota':"fachri"})
        self.assertEqual(response.status_code, 200)


    #Test Forms
    def test_form_aktivitas(self):
        form = FormAktivitas()
        form_data = {'aktivitas':'nongki'}
        form = FormAktivitas(data=form_data)
        self.assertTrue(form.is_valid())
    
    def test_form_peserta(self):
        form = FormPeserta()
        form_data = {'peserta':'Udin', 'kegiatan': 'kayang'}
        form = FormPeserta(data=form_data)
        self.assertTrue(form.is_valid())