from django.contrib import admin
from .models import Aktivitas, Peserta

# Register your models here.
admin.site.register(Aktivitas)
admin.site.register(Peserta)