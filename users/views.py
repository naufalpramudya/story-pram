from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.


def user(request):
    return render(request, 'users/user.html')


def signup(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        if not username or not password:
            messages.error(
                request, "Please enter username / password correctly")
            return HttpResponseRedirect('/user/signup/')
        if User.objects.filter(username__iexact=username).exists():
            messages.error(request, "Username taken")
            return HttpResponseRedirect('/user/signup/')
        else:
            user = User.objects.create_user(
                username=username, password=password)
            user.save()
            messages.success(request, 'Account Successfully Created!')
            return HttpResponseRedirect('/user/login/')
    else:
        return render(request, 'users/signup.html')


def log_in(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        if not username or not password:
            messages.error(
                request, "Please enter username / password correctly")
            return HttpResponseRedirect('/user/login/')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/user/')
        else:
            messages.error(
                request, "Wrong credentials (Wrong Username / Password)")
            return HttpResponseRedirect('/user/login/')
    else:
        return render(request, 'users/login.html')


def log_out(request):
    logout(request)
    messages.success(request, 'Log out Success')
    return render(request, 'users/login.html')


def bio(request):
    if request.method == 'POST':
        user = User.objects.get(pk=request.POST['id'])
        user.username = request.POST['username']
        user.profile.firstname = request.POST['firstname']
        user.profile.lastname = request.POST['lastname']
        user.profile.bio = request.POST['bio']
        user.save()
        return HttpResponseRedirect('/user/')

    return render(request, 'users/bio.html')
