from django.test import TestCase, Client
from django.urls import resolve
from .views import user, signup, log_in, bio, log_out
# Create your tests here.


class UnitTest(TestCase):

    # Test User Page
    def test_url_user(self):
        response = Client().get('/user/')
        self.assertEquals(response.status_code, 200)

    def test_function_user(self):
        view = resolve('/user/')
        self.assertEquals(view.func, user)

    def test_template_user(self):
        response = Client().get('/user/')
        self.assertTemplateUsed(response, 'users/user.html')

    # Test Sign up page
    def test_url_signup(self):
        response = Client().get('/user/signup/')
        self.assertEquals(response.status_code, 200)

    def test_function_signup(self):
        view = resolve('/user/signup/')
        self.assertEquals(view.func, signup)

    def test_template_signup(self):
        response = Client().get('/user/signup/')
        self.assertTemplateUsed(response, 'users/signup.html')

    # Test Login Page
    def test_url_login(self):
        response = Client().get('/user/login/')
        self.assertEquals(response.status_code, 200)

    def test_function_login(self):
        view = resolve('/user/login/')
        self.assertEquals(view.func, log_in)

    def test_template_login(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response, 'users/login.html')

    # Test Bio Page
    def test_url_bio(self):
        response = Client().get('/user/bio/')
        self.assertEquals(response.status_code, 200)

    def test_function_bio(self):
        view = resolve('/user/bio/')
        self.assertEquals(view.func, bio)

    def test_template_bio(self):
        response = Client().get('/user/bio/')
        self.assertTemplateUsed(response, 'users/bio.html')

    # Test Logout
    def test_url_logout(self):
        response = Client().get('/user/logout/')
        self.assertEquals(response.status_code, 200)

    def test_function_logout(self):
        view = resolve('/user/logout/')
        self.assertEquals(view.func, log_out)
