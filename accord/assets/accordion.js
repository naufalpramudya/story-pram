$(document).ready(function(){
    $('.card-header').click(function(){
        if ($(this).next('.card-body').hasClass('active')){
            $(this).next('.card-body').removeClass('active').slideUp();
            $(this).css('background-color','lightskyblue');
            $(this).children('span').removeClass('fa-minus').addClass('fa-plus');
        }
        else{
            $('.card .card-body').removeClass('active').slideUp();
            $('.card-header').css('background-color','lightskyblue');
            $('.card .card-header span').removeClass('fa-minus').addClass('fa-plus');
            $(this).next('.card-body').addClass('active').slideDown();
            $(this).css('background-color','lightgreen');
            $(this).children('span').removeClass('fa-plus').addClass('fa-minus');
            
        }
    })
    $('.card-header .btn-group .down').click(function (e) {
        var self = $(this),
            item = self.parents('div.card'),
            swapWith = item.next();
        item.before(swapWith.detach());
    });
    $('.card-header .btn-group .up').click(function (e) {
        var self = $(this),
            item = self.parents('div.card'),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });

})