from django.test import TestCase, Client
from django.urls import resolve
from .views import accordion


# Create your tests here.
class UnitTest(TestCase):
    def test_url_accord(self):
        response = Client().get('/accord/')
        self.assertEquals(response.status_code, 200)
    
    def test_function_accord(self):
        view = resolve('/accord/')
        self.assertEquals(view.func, accordion)

    def test_template_accord(self):
        response = Client().get('/accord/')
        self.assertTemplateUsed(response, 'accord/accordion.html')